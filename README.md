# Vérification des journaux des conteneurs

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

Nous allons parler de la vérification des journaux des conteneurs. Voici un aperçu rapide de ce dont nous allons discuter :

- Journaux des conteneurs à un niveau élevé
- La commande `kubectl logs`
- Démonstration pratique

# Journaux des conteneurs

Les conteneurs Kubernetes maintiennent tous des journaux, que vous pouvez utiliser pour comprendre ce qui se passe à l'intérieur du conteneur. Le journal d'un conteneur contient essentiellement tout ce qui est écrit dans les flux de sortie standard (stdout) et d'erreur standard (stderr) par le processus du conteneur.

# La commande `kubectl logs`

Nous pouvons utiliser la commande `kubectl logs` pour afficher les journaux d'un conteneur. Cette commande est assez simple : il suffit de taper `kubectl logs` suivi du nom du pod. Si le pod a plus d'un conteneur, vous devez spécifier le conteneur pour lequel vous souhaitez obtenir les journaux en utilisant l'option `-c`. Bien sûr, si le pod n'a qu'un seul conteneur, cette option est facultative.

# Démonstration pratique

Voyons maintenant une démonstration pratique de la manière dont nous pouvons collecter les journaux des conteneurs dans Kubernetes. Nous sommes connectés à notre nœud de plan de contrôle, et nous allons créer un fichier YAML simple pour créer ce pod.

Créez un fichier YAML, par exemple `logs-pod.yml`, et collez-y le contenu suivant :

```bash
nano logs-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: logs-pod
spec:
  containers:
  - name: busybox
    image: busybox
    command: ['sh', '-c', 'while true; do echo "here is my output"; sleep 5; done']
```

Ce pod utilise une image BusyBox et, toutes les 5 secondes, il écrit "here is my output". Ce texte qui est échoé ira dans le flux de sortie standard (stdout), ce qui signifie qu'il finira dans notre journal de conteneur.

Enregistrez et quittez le fichier, puis créez le pod avec la commande suivante :

```sh
kubectl apply -f logs-pod.yml
```

>![Alt text](img/image.png)
*Pod crée*

Maintenant, nous devrions pouvoir utiliser la commande `kubectl logs` :

```sh
kubectl logs logs-pod
```

Comme ce pod n'a qu'un seul conteneur, nous n'avons pas besoin de spécifier l'option `-c`. Si nous avions plusieurs conteneurs, nous pourrions spécifier le conteneur comme ceci :

```sh
kubectl logs logs-pod -c busybox
```

>![Alt text](img/image-1.png)
*Sortie de la commande*

Lorsque vous exécutez la commande, vous devriez voir plusieurs lignes indiquant "here is my output". Cela vous donne une idée de ce qui se passe lorsque nous utilisons `kubectl logs`.

# Récapitulatif

Dans cette leçon, nous avons parlé des journaux des conteneurs à un niveau élevé. Nous avons brièvement discuté de la commande `kubectl logs`, et nous avons fait une démonstration pratique de la manière dont nous pouvons accéder aux journaux des conteneurs dans Kubernetes. C'est tout pour cette leçon. Nous vous verrons dans la prochaine leçon.


# Reférence

https://kubernetes.io/docs/tasks/debug/debug-application/debug-running-pod/